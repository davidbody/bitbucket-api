import requests
import config

def get_repositories():
    resp = requests.get('https://api.bitbucket.org/2.0/repositories/{0}'.
                        format(config.account_or_team), auth=(config.user, config.password))
    return resp.json()['values']

def get_branches(repo_name):
    resp = requests.get('https://api.bitbucket.org/1.0/repositories/{0}/branches'.
                        format(repo_name), auth=(config.user, config.password))
    return resp.json()

def main():
    repositories = get_repositories()

    for r in repositories:
        repo_name = r['full_name']
        clone_url = r['links']['clone'][1]['href']
        branches = get_branches(repo_name)
        print("Repository: " + repo_name)
        print("Clone URL: " + clone_url)
        print("Branches (SHA-1s):")
        for b in branches:
            print('  {0} {1}'.format(b, branches[b]['node']))
            print()

if __name__ == '__main__':
    main()
