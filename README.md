# Bitbucket API example

This example uses the Requests library.

    :::bash
        $ pip install requests

Bitbucket credentials go in `config.py`. Copy `config.py.example` to `config.py` and edit.

Then run

    :::bash
        $ python bitbucket_repos.py

You should see a list of Bitbucket repositories and a list of branches and their SHA-1's for each repository.
